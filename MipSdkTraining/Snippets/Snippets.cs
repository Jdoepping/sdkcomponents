﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using Rectangle = VideoOS.Platform.Metadata.Rectangle;

namespace MipSdkTraining
{

    /// <summary>
    /// Introduces:
    /// Configuration.Instance
    /// Item (focus on GetChildren methods)
    /// Kind
    /// FQID
    /// Tree-structure of the configuration. 
    /// </summary>
    public partial class MipTrainingTest
    {

        /// <summary>
        /// Observe a List of Items are returned. 
        /// </summary>
        [TestMethod]
        public void GetCamerasTest()
        {
            Logon();
            var cams = Configuration.Instance.GetItemsByKind(Kind.Camera);
            Assert.IsTrue(cams.Count > 0);
        }

        [TestMethod]
        public void IsFirstItemOfKindCameraTest()
        {
            Logon();
            var cams = Configuration.Instance.GetItemsByKind(Kind.Camera);
            var cam01 = cams.First();
            Assert.IsTrue(cam01.FQID.Kind == Kind.Camera);
        }

        [TestMethod]
        public void HierarchyTest()
        {
            Logon();
            // Level 1: Server
            var serverList = Configuration.Instance.GetItemsByKind(Kind.Camera);
            var server = serverList.First();
            Assert.IsTrue(server.FQID.Kind == Kind.Server);
            Assert.IsTrue(server.GetChildren().Any());
            var children = server.GetChildren();
            // Level 2: Apparently a container 
            var firstChild = children.First();
            Assert.IsTrue(firstChild.FQID.Kind == Kind.Camera);
            Assert.IsFalse(firstChild.Name.Contains("Camera 1"));
            // Level 3: Finally the camera
            var firstCam = children.First();
            Assert.IsTrue(firstCam.FQID.Kind == Kind.Camera);
        }

        [TestMethod]
        public void GetFlatCameraListTest()
        {
            Logon();
            var parent = Configuration.Instance.GetItemsByKind(Kind.Camera);
            var cams = Cameras(parent);
            Assert.IsTrue(cams.First().Name.Contains("Camera 1"));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private List<Item> Cameras(List<Item> parent = null)
        {
            var cams = new List<Item>();
            if (parent == null) return cams;
            foreach (var item in parent)
            {
                if (IsACamera(item)) cams.Add(item);
                else
                    cams.AddRange(Cameras(item.GetChildren()));
            }
            return cams;
        }

        private bool IsACamera(Item item) => item.FQID.Kind == Kind.Camera && item.FQID.FolderType == FolderType.No && item.FQID.Kind == Kind.Camera;

        private void WriteOverlayText(ImageViewerAddOn iv, string overlayText)
        {
            var bitmap = new Bitmap(iv.Size.Width, iv.Size.Height);
            var g = Graphics.FromImage(bitmap);
            var brush = new SolidBrush(Color.Transparent);
            g.FillRegion(brush, new Region(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height)));

            var writeFont = new Font("Arial", 18);
            var writeBrush = new SolidBrush(Color.GreenYellow);
            g.DrawString(overlayText, writeFont, writeBrush, new PointF(50.0F, 50.0F));
            iv.SetOverlay(bitmap, 1, false, false, true, 1.0,
                System.Windows.Forms.DockStyle.None,
                System.Windows.Forms.DockStyle.None, 0.0, 0.0);
            g.Dispose();
            bitmap.Dispose();
        }
    }
}
