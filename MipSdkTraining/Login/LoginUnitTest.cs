﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MipSdkTraining
{
    /* 
     * 1. Enter credentials that makes the tests run
     * 2. Try stopping the Management Server, what happens?
     * 
     */
    [TestClass]
    public partial class MipTrainingTest
    {
        private Uri uri;

        /// <summary>
        /// A rename to clarify the intend. 
        /// </summary>
        private  void Logon() => CurrentWindowsUserTest();

        [TestInitialize]
        public void Setup()
        {
            // Must initialize the environment when in a stand alone application
            VideoOS.Platform.SDK.Environment.Initialize();
            uri = new Uri("http://localhost");
        }
        /// <summary>
        /// The management server is resposible for handling login and authentication.
        /// 3 different ways credentials can be provided.
        /// 1. Current windows user. Credentials are "" for username and password. The windows user must
        ///    also be a Milestone user
        /// 2. Use negotiate with username and password
        /// 3. Basic user
        /// </summary>
        [TestMethod]
        [TestCategory("Login-Authentication")]
        public void CurrentWindowsUserTest()
        {
            // Observe!
            // the credentials are "" for username and pw.
            var credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, "", "", "Negotiate");

            Common(credentialCache);
        }

        [TestMethod]
        [TestCategory("Login-Authentication")]
        public void SpecifyingWindowsCredentialsUserTest()
        {
            // Observe!
            // 1. username and password are empty
            // 2. The method BuildNetworkCredential is used, compare with "BasicUserTest"
            // 3. Observe the usage of "uri"
            System.Net.NetworkCredential credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, @"jad", "password", "Negotiate");

            Common(credentialCache);
        }

        [TestMethod]
        [TestCategory("Login-Authentication")]
        public void BasicUserTest()
        {
            // Observe!
            // 1. Basic is used not negotiate
            // 2. The method BuildCredentialCache is used, compare with the windows users tests
            // 3. Observe the usage of "uri"
            System.Net.CredentialCache credentialCache = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, "Jakob", "Jakob", "Basic");

            Common(credentialCache);
        }

        /// <summary>
        /// The parameter credentialCache is either:
        /// 1. System.Net.CredentialCache
        /// 2. System.Net.NetworkCredential
        /// </summary>
        /// <param name="credentialCache"></param>
        private void Common(dynamic credentialCache)
        {
            VideoOS.Platform.SDK.Environment.RemoveAllServers();
            // Observe the usage of uri!
            VideoOS.Platform.SDK.Environment.AddServer(uri, credentialCache);

            try
            {
                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (Exception ex)
            {
                Assert.Fail($"Login failed: {ex.Message}");
            }
            Assert.IsTrue(true);
        }
    }
}
