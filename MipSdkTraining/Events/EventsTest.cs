﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MipSdkTraining.Messaging;
using VideoOS.Platform;
using VideoOS.Platform.Data;
using VideoOS.Platform.Messaging;

namespace MipSdkTraining.Events
{
    [TestClass]
    public class EventTest
    {
        [TestInitialize]
        public void Setup()
        {
            VideoOS.Platform.SDK.Environment.Initialize();
            if (!MessagingTest.Login("Negotiate")) // either Basic or Negotiate
            {
                throw new Exception("Login failed");
            }
        }

        /// <summary>
        /// User defined events becomes available in the smart client! (Live, side panel Events )
        /// Attach the event to a rule or an alarm.
        /// Add the event "MyUserDefinedEvent" as an userdefined-event.
        /// (Added a rule that start recording on the "RaiseGenericEvent" and stops recording on "RaiseUserDefinedEvent")
        /// Observe the rather confusing usage of GetItemsByKind! It does not just return a list of the Kind.TriggerEvent.
        /// </summary>
        [TestMethod]
        public void RaiseUserDefinedEventTest()
        {
            var top = Configuration.Instance.GetItemsByKind(Kind.TriggerEvent).First();
            foreach (var child in top.GetChildren())
            {
                foreach (var grandChild in child.GetChildren())
                {
                    if (!string.Equals(grandChild.Name, "All EventTypes (Corporate)", StringComparison.CurrentCultureIgnoreCase)) continue;
                    foreach (var userDefinedEvents in grandChild.GetChildren())
                    {
                        if (!string.Equals(userDefinedEvents.Name, "MyUserDefinedEvent", StringComparison.CurrentCultureIgnoreCase)) continue;
                        EnvironmentManager.Instance.SendMessage(new Message(MessageId.Control.TriggerCommand), userDefinedEvents.FQID);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// using generic events
        /// remember to enable generic events in MC/tools
        /// remember to set the expression to "open door" (without "" though!!!!) in the generic event definition.
        /// can be used by the rule system or alarm system or consumed by a plug-in. 
        /// Use the rule engine start and stop recording.  
        /// </summary>
        [TestMethod]
        public void RaiseGenericEventTest()
        {
            try
            {
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var ipaddr = IPAddress.Parse("10.10.48.117");
                var endpoint = new IPEndPoint(ipaddr, 1234);
                socket.Connect(endpoint);
                var toSend = "open door";
                byte[] bytesSent = Encoding.ASCII.GetBytes(toSend);
                socket.Send(bytesSent, bytesSent.Length, 0);
                byte[] bytesReceived = new Byte[bytesSent.Length];
                socket.Receive(bytesReceived);
                socket.Close();
                // In the MC generic events are set to ecco back what was send.
                Assert.IsTrue(System.Text.Encoding.ASCII.GetString(bytesReceived) == toSend);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        /// <summary>
        /// Setup: Add an analytical event in the MC. Add an alarm in the MC that is raised when 
        /// the analytical event is raised. 
        /// The name of the analytical event is used in the EventHeader.Message property.
        /// The vms listens to the MessageId.Server.NewEventCommand and others and because of what
        /// is in the Data property the event is then identified as an analytical event.
        /// Remember that analytical events must be enabled in the MC.
        /// 
        /// The difference between events is what the vms can do with the event information.
        /// Analytical events can include bounding boxes and other information. But can only react 
        /// natively on for example bounding boxes.
        ///
        /// The MessageId.Server.NewEventCommand is received and handled by the system. So,
        /// the local system (on this machine) accepts the message.
        /// </summary>
        [TestMethod]
        public void RaiseAnalyticalEventTest()
        {
            var cam = Configuration.Instance.GetItem(MessagingTest.GetCameraGuid(), Kind.Camera);
            var eventSource = new EventSource()
            {
                // Send empty - it is possible to send without an eventsource, but the intended design is that there should be a source
                // the FQID is primarily used to match up the ObjectId with a camera.
                FQID = cam.FQID,
                // If FQID is null, then the Name can be an IP address, and the event server will make a lookup to find the camera
                Name = cam.Name
            };

            var eventHeader = new EventHeader
            {
                ID = Guid.NewGuid(),
                Type = "MyType",
                Timestamp = DateTime.Now,
                Message = "HelloFromUnitTestAnalyticalEvent", // Analytical Event Id!
                Source = eventSource,
                CustomTag = "TagFromC#"
            };

            var analyticsEvent = new AnalyticsEvent
            {
                EventHeader = eventHeader,
                Location = "Event location 1",
                Description = "Analytics event description.",
                Vendor = new Vendor { Name = "My Smart Video" },
            };
            EnvironmentManager.Instance.SendMessage(new Message(MessageId.Server.NewEventCommand) { Data = analyticsEvent });
            //MessageId.Server.NewEventIndication
            // Can I listen to my own event? 
        }

        /// <summary>
        /// The below is wrong! This event is only available from a smart client plug-in.
        /// Observe that it is a background plug-in loaded by the Smart Client.
        /// Remember to change the TargetEnvironments setting to include EnvironmentType.SmartClient.
        /// It doesn't have to be a background plug-in, the line below can be loaded in plug-in loaded by the smart client.
        /// Find Logger in Client.exe.config file and uncomment the two lines there. 
        /// </summary>
        [TestMethod]
        public void SubscribeToNewImageViewerControlEventTest()
        {
            ClientControl.Instance.NewImageViewerControlEvent += Instance_NewImageViewerControlEvent;
            // Get a no badimage exception! What a surprise!
            VideoOS.Platform.SDK.UI.Environment.Initialize();
            var ic = ClientControl.Instance.GenerateImageViewerControl();

            var count = 0;
            while (count < 200)
            {
                Thread.Sleep(100);
            }
            Assert.Fail("Is the NewImageViewerControlEvent available outside the smart client?");
        }

        private void Instance_NewImageViewerControlEvent(VideoOS.Platform.Client.ImageViewerAddOn imageViewerAddOn)
        {
            throw new NotImplementedException();
        }


    }
}
