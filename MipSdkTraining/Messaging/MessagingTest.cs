﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace MipSdkTraining.Messaging
{
    /* Trying both types of messaging EnvironmentManager Messaging and MipMessaging.
     * 1. Enter working credentials in the Login method below.
     * 2. Make TestSendMethod() pass (become green)
     * 3. Where does the returnvalue come from?
     * 4. Does the async version have a return value?
     * 5. When the test is run here is it then a stand alone app? How would you check it (Check EnvironmentManager properties)?
     * 6. Is it possible to message between two stand alone apps using EnvironementManager messaging? 
     *    (How can it be tested?)  
     * 7. Look at TestStartStopRecording(). This test introduces messaging to the Milestone VMS! and Usage of Configuration.Instance.
     * 8. What type is "cam"? Decomment the line: Assert.IsInstanceOfType(cam, typeof(????)); and put in the correct type.
     * 9. Check whether the camera has started to record.
     * 10. Make a test that stops the recording.
     * 11. Does the camera support PTZ? Does it have "EdgeSupport" (Hint. Consider Item.Properties) (What is edge support?)
     * 12. MessageId.SmartClient.GetCurrentPlaybackTimeRequest
     * 
     * ------------------------------ Mip Messaging -------------------------------------------------------
     * 1. Make TestSendMipMessage green (make it pass)
     * 2. Verify that MIP messaging between apps is possible.
     * 3. Stop the event server service and retry the TestSendMipMessage.
     */
    /// <summary>
    /// Sample: MessageTester
    ///
    /// Help: Search for "Messaging Command" or "Introduction to MIP Message Communication"
    /// </summary>
    [TestClass]
    public class MessagingTest
    {

        private string mipMessagingReceiverMarker = "Nothing Received waiting...";
        private bool messageReceived;

        [TestInitialize]
        public void Setup()
        {
            VideoOS.Platform.SDK.Environment.Initialize();
            if (!Login("Negotiate")) // either Basic or Negotiate
            {
                throw new Exception("Login failed");
            }
        }

        /// <summary>
        /// Talks to itself
        /// The scope of this messaging is the current application. 
        /// So it cannot message from one stand alone to another stand alone app or from one 
        /// smart client to another.
        ///
        /// Observe:
        /// The steps
        /// 1. Define a messageId (here: "com.Milestone.SdkTraining.MessageIdSample")
        /// 2. Register to listen to the message
        ///
        /// 3. Send the message, the message can be send regardless of whether or not step 1 and 2 are performed
        ///
        /// 4. Un-register when listening is no longer required.    
        /// </summary>
        [TestMethod]
        [TestCategory("Messaging")]
        public void TestSendMethod()
        {
            // Define a message id
            var messageId = "com.Milestone.SdkTraining.MessageIdSample";
            // start listening
            var obj = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(messageId));
            // waits for all registered receivers to reply! 
            // MISSING: Send a message to yourself!
            var returnvalue = EnvironmentManager.Instance.SendMessage(new Message(messageId));
            Assert.IsTrue((string)returnvalue.First() == "MessageReceived");
            var type = EnvironmentManager.Instance.EnvironmentType;
            // Don't introduce memoryleaks...
            EnvironmentManager.Instance.UnRegisterReceiver(obj);
        }

        [TestMethod]
        [TestCategory("Messaging")]
        public void TestReceive()
        {
            // Define a message id
            var messageId = MessageId.Server.NewEventIndication; //"com.Milestone.SdkTraining.MessageIdSample";
            // start listening
            var obj = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(messageId));
            while (!messageReceived)
            {
                Thread.Sleep(1000);
            }
        }

        [TestMethod]
        [TestCategory("Messaging")]
        public void TestSend()
        {
            // Define a message id
            var messageId = MessageId.Server.NewEventIndication;//"com.Milestone.SdkTraining.MessageIdSample";

            var returnvalue = EnvironmentManager.Instance.SendMessage(new Message(messageId));

        }


        /// <summary>
        /// Message handler
        /// </summary>
        /// <param name="message"></param>
        /// <param name="destination"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        private object ReceiveMyMessagePlease(Message message, FQID destination, FQID sender)
        {
            return "MessageReceived";
        }

        #region MIP messaging (Large scope)

        private class MyClass
        {
        }


        /// <summary>
        /// Using MIP messaging enables messaging across apps from the smart client to the management client to standalone apps.
        /// But only within the same VMS (The vms the event server belongs to)
        /// The event server is responsible for servicing MIP messaging. 
        /// Try to close down the event server and see what happens.
        /// </summary>
        [TestMethod]
        [TestCategory("Messaging")]
        public void TestSendMipMessage()
        {
            // Make the initial mysterious incantation
            var communicator = SetupCommunication();


            var messageId = "com.Milestone.SdkTraining.MipMessage";
            var obj = communicator.RegisterCommunicationFilter(ReceiveMyMipMessagePlease, new CommunicationIdFilter(messageId));
            // !!!!! Some time must be allowed for the registration to finish. !!!!
            Thread.Sleep(10000);
            // Observe the difference TransmitMessage does not produce a result!
            //var myItem = new Item(new FQID(new ServerId("type","host","prot",Guid.NewGuid())),"MyItem");

            communicator.TransmitMessage(new Message(messageId, new MyClass()), null, null, null);

            #region Waiting for the message to be recieved
            var count = 0;
            var received = false;
            while (!received && count < 100)
            {
                received = mipMessagingReceiverMarker == "Received Mip Message";
                count++;
                Thread.Sleep(500);
            }
            #endregion Waiting for the message to be recieved

            communicator.UnRegisterCommunicationFilter(obj);
            Assert.IsTrue(received);
        }

        /// <summary>
        /// Use this test to demonstrate cross application messaging.
        /// Start another visual studio show that inter process messaging is possible.
        /// Make one of the Visual studios compile in release mode and the other in debug, otherwise running the second will fail because
        /// the dlls are loaded by the first and cannot be overwritten.
        /// </summary>
        [TestMethod]
        [TestCategory("Messaging")]
        public void TestRunMeInAnotherProcess()
        {
            // Make the initial mysterious incantation
            var communicator = SetupCommunication();

            var messageId = "com.Milestone.SdkTraining.MipMessage";
            var obj = communicator.RegisterCommunicationFilter(ReceiveMyMipMessagePlease, new CommunicationIdFilter(messageId));
            //communicator.TransmitMessage(new Message(messageId), null, null, null);
            var count = 0;
            var received = false;
            while (!received && count < 2000) // runs for 2 minutes, unless the message is received  
            {
                received = mipMessagingReceiverMarker == "Received Mip Message";
                count++;
                Thread.Sleep(500);
            }
            //communicator.UnRegisterCommunicationFilter(obj);
            Assert.IsTrue(received);
        }


        private object ReceiveMyMipMessagePlease(Message message, FQID destination, FQID sender)
        {
            mipMessagingReceiverMarker = "Received Mip Message";
            return "MessageReceived";
        }

        private MessageCommunication SetupCommunication()
        {
            // Make the initial mysterious incantation
            var serverId = EnvironmentManager.Instance.MasterSite.ServerId;
            MessageCommunicationManager.Start(serverId);
            var communicator = MessageCommunicationManager.Get(serverId);
            return communicator;
        }



        #endregion MIP messaging (Large scope)

        #region helper methods
        public static bool Login(string negotiateOrBasic)
        {
            var uri = new Uri("http://localhost");
            dynamic credentialCache;
            switch (negotiateOrBasic)
            {
                case "Basic":
                    credentialCache = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, "Jakob", "Jakob", "Basic");
                    break;
                case "Negotiate":
                    credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, "", "", "Negotiate");
                    break;
                default: throw new ArgumentException($"Invalid argument: {negotiateOrBasic}");
            }

            VideoOS.Platform.SDK.Environment.RemoveAllServers();
            VideoOS.Platform.SDK.Environment.AddServer(uri, credentialCache);

            try
            {
                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Replace the below guid, with the one from your Mip Driver cam.
        /// Goto the MC and Recording servers, find the  Mip Driver cam. Make sure to select the camera,
        /// not the general hardware (top level node) 
        /// look at the properties, hold down the ctrl key and select the "info" tab.
        /// Select the guid id and replace. 
        /// </summary>
        /// <returns></returns>
        public static Guid GetCameraGuid()
        {
            return new Guid("{04ADEDD3-0FB1-43C4-B294-99DF52DD3EFE}");
        }
        #endregion helper methods
    }
}
