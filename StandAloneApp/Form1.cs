﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.UI;
using Message = VideoOS.Platform.Messaging.Message;

namespace StandAloneApp
{
    /// <summary>
    /// Introduces:
    /// ClientControl.Instance  (Client=SmartClient)
    /// ImageViewerControl
    ///
    /// EnvironmentManager.Instance (Environment related)
    /// Messaging sending commands and getting information.
    /// 
    /// Build in message ids
    ///
    /// PlaybackController
    ///
    /// Use of FQID and not the actual item eg. the FQID for camera and playbackController.
    ///
    /// ItemPickerForm (this can be done in a unit test)
    ///
    /// Focus on
    /// ClientControl.Instance.
    /// Generate methods
    /// CallOnUiThread() method (not used so far)
    /// 
    ///
    /// Samples: SCIndependentPlayback, MediaPlayback
    /// </summary>
    public partial class Form1 : Form
    {
        private FQID _playbackFQID;
        /// <summary>
        /// To make this example run some dlls must be copied to the debug folder:
        /// C:\Program Files\Milestone\MIPSDK\Bin>copyMedia.Bat "destination folder"
        ///
        /// Change to x64 configuration to avoid the below error.
        /// System.BadImageFormatException: 'Could not load file or assembly 'VideoOS.Platform.SDK
        ///
        /// This example is a reduced version of the "VideoViewer" sample. 
        /// 
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            VideoOS.Platform.SDK.Environment.Initialize();
            VideoOS.Platform.SDK.UI.Environment.Initialize();
            CurrentWindowsUserLogin();

            MyImageViewerControl = ClientControl.Instance.GenerateImageViewerControl();

            MyImageViewerControl.BackColor = Color.Aqua;
            MyImageViewerControl.Dock = DockStyle.Fill;
            ImageViewerContainerPanel.Controls.Add(MyImageViewerControl);
            // Initialize here is impossible
            //MyImageViewerControl.Initialize();
            //MyImageViewerControl.Connect();
            //MyImageViewerControl.Selected = true;
        }

        public Panel ViewerPanel => ImageViewerContainerPanel;
        public ImageViewerControl MyImageViewerControl { get; set; }
        public ImageViewerControl MyImageViewerControl1 { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            MyImageViewerControl.Initialize();
            MyImageViewerControl.Connect();
            return;

            var form = new ItemPickerForm();
            form.KindFilter = Kind.Camera;
            form.AutoAccept = true;
            form.Init(Configuration.Instance.GetItems());
            form.ShowDialog();
            var camera = form.SelectedItem;
            
            MyImageViewerControl.CameraFQID = camera.FQID;
            MyImageViewerControl.Initialize();
            MyImageViewerControl.Connect();
            MyImageViewerControl.Selected = true;
        }
        public static void CurrentWindowsUserLogin()
        {
            // Observe!
            // the credentials are "" for username and pw.
            var uri = new Uri("http://localhost");
            var credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, "", "", "Negotiate");

            VideoOS.Platform.SDK.Environment.RemoveAllServers();
            // Observe the usage of uri!
            VideoOS.Platform.SDK.Environment.AddServer(uri, credentialCache);

            try
            {
                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (Exception ex)
            {
                throw new Exception($"Login failed: {ex.Message}");
            }
        }

        private List<Item> Cameras(List<Item> parent = null)
        {
            var cams = new List<Item>();
            foreach (var item in parent)
            {
                if (IsACamera(item)) cams.Add(item);
                else
                    cams.AddRange(Cameras(item.GetChildren()));
            }
            return cams;
        }



        private bool IsACamera(Item item) => item.FQID.Kind == Kind.Camera && item.FQID.FolderType == FolderType.No;


        /*
         * In help search for: "Working with Messages" 
         */

        /// <summary>
        /// Use EnvironmentManager.Instance.SendMessage to send a message to the local system.
        /// Observe: The message is send without a "Destination". The function signature
        /// SendMessage(Message) is used. This means that the destination will have the
        /// value of null.
        /// We have not specified a PlaybackController for the ImageViewControl (check it in the constructor)
        /// The system has a "default" PlaybackController with FQID = null. This playbackcontroller controls
        /// what is shown!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            var message = new Message(MessageId.System.ModeChangeCommand, Mode.ClientPlayback);

            EnvironmentManager.Instance.SendMessage(message/*, _playbackFQID*/);
            //EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler, new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
        }

        private object PlaybackTimeChangedHandler(Message message, FQID destination, FQID sender)
        {
            return null;
        }

        private void btnSetTime_Click(object sender, EventArgs e)
        {
            var message = new Message(
                MessageId.SmartClient.PlaybackCommand,
                new PlaybackCommandData { Command = PlaybackData.PlayStop });

            EnvironmentManager.Instance.SendMessage(message, _playbackFQID);

            EnvironmentManager.Instance.SendMessage(new Message(MessageId.SmartClient.PlaybackCommand,
                    new PlaybackCommandData()
                    {
                        Command = PlaybackData.Goto,
                        DateTime = DateTime.Now.AddMinutes(-10), // Set time to 10 minutes earlier. 
                    }),
                _playbackFQID);

            EnvironmentManager.Instance.SendMessage(new Message(MessageId.SmartClient.PlaybackCommand,
                new PlaybackCommandData
                { Command = PlaybackData.PlayForward, Speed = 1.0 }),
                _playbackFQID);
        }

        /// <summary>
        /// Getting information from the system.
        /// Observe that the "SendMessage" method has a return type of collection of object.
        ///
        /// We can request information from the system. Below is a request to get the GetCurrentPlaybackTimeRequest.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetCurrentPLayBackTime_Click(object sender, EventArgs e)
        {
            var playBackTime = EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.SmartClient.GetCurrentPlaybackTimeRequest));
            if (playBackTime.Any())
            {
                txtPlayBackTime.Text = ((DateTime)playBackTime.First()).ToLocalTime().ToLongTimeString();
            }
            #region Attempts to get info that is not available in a standalone app 

            // All of the below returns nothing. 
            var cam = EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.SmartClient.GetSelectedCameraRequest, _playbackFQID));
            var ws = EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.SmartClient.GetCurrentWorkspaceRequest, _playbackFQID));
            var ptz = EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.Control.PTZGetAbsoluteRequest, MyImageViewerControl.CameraFQID));
            var ip = EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.Server.GetIPAddressRequest, MyImageViewerControl.CameraFQID), MyImageViewerControl.CameraFQID);

            #endregion Attempts to get info that is not available in a standalone app 


        }

        private void button4_Click(object sender, EventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(MessageId.System.ModeChangeCommand,
                Mode.ClientLive), _playbackFQID);
        }
    }
}
